import Vue from 'vue'
import VueResource from "vue-resource"
import App from "components/App.vue"
// import Routes from "config/routes" // old route config
import appRoutes from "config/appConfig"
const Routes = appRoutes.routes
import VueRouter from "vue-router"
import lazyload from 'vue-lazyload'

/* ----------  components  ---------- */

// find page components/routes in config folder

/* ----------  Installs  ---------- */

Vue.use(VueRouter);
Vue.use(VueResource);
Vue.use(lazyload,{
	error: './img/utility/error.png',
	loading: './img/utility/hex-loader.gif',
	try: 1
});

Vue.config.debug = true;

export var router = new VueRouter({
	el: "#app",
	root: "/",
	props: ['business'],
	history: true,
	hashbang: false,
	transitionOnLoad: true
});
/* ----------  Match vue routes  ---------- */

router.mode = "html5";

router.map(Routes);

// scroll to the top of the screen each time we redirect

router.beforeEach(function() {
  window.scrollTo(0, 0);
	// document.getElementById("app").scrollTop = 0
});

// redirect to home if route not matched

router.redirect({
  "*": "/home"
});

// start app on the #app-main elelement
router.start(App, "#app");
