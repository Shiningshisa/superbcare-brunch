import Home from "../components/pages/Home.vue";
import About from "../components/pages/About.vue";
import Services from "../components/pages/Services.vue";
import Wrok from "../components/pages/Work.vue";
import Help from "../components/pages/Help.vue";
import Contact from "../components/pages/Contact.vue";


module.exports = {

  business:{
    title: 'Superb Health care',
    business_registration_no: 'unknown',
    owner: 'Peter Kiramba'
  },

  routes:{
    "/home": {
  		name: "home",
  		component: Home,
  		blurb: 'Our home page'
  	},
  	"/about": {
  		name: "about",
  		component: About,
  		blurb: `Leanr about Superb Healthcare Ltd`
  	},
  	"/services": {
  		name: "services",
  		component: Services,
  		blurb: 'Learn about our services'
  	},
  	"/work": {
  		name: "work",
  		component: Wrok,
  		blurb: 'See our vacancies'
  	},
  	"/help": {
  		name: "help",
  		component: Help,
  		blurb: 'See a list of frequently asked questions'
  	},
  	"/contact": {
  		name: "contact",
  		component: Contact,
  		blurb: 'How to get in touch with us'
  	}
  }

}