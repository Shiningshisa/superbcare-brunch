module.exports = {
    files: {
        javascripts: {
            joinTo: {
                "app.js": /^app/,
                "vendor.js": /^(?!app)/
            }
        },
        templates: {
            joinTo: "app.js"
        },
        stylesheets: {
            joinTo: "stylesheets/app.css"
        }
    },

    plugins: {
        babel: {
            presets: ["es2015"]
        }
    }
};
