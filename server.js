var express = require('express');
var path = require('path');
var appPort = process.env.PORT || 6256;
var dir = require('node-dir');
var app = express();

app.use(express.static('public'));

app.get('/', function(req, res){
  res.sendFile(__dirname +  'public/index.html');
});

app.listen(appPort, function(err){
  if(err) {console.log(err) };
  console.log('Listening on port localhost: ' + appPort);
});

module.exports = function() {
    return app;
}